export const APP_ANDROID_VERSION = '1.2.0';
export const GOOGLEPLAY = '';
export const APP_IOS_VERSION = '1.2.0';
export const APPSTORE = '';
export const SOURCE_URL =
  'https://gitlab.com/blurt/blurt/-/tree/dev/ui/letsblurt';
export const TERMS_URL = 'https://letsblurt.app/terms';
export const PRIVACY_URL = 'https://letsblurt.app/privacy';
